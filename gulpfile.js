/*!
 * This file is part of help.eyeo.com.
 * Copyright (C) 2017-present eyeo GmbH
 *
 * help.eyeo.com is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * help.eyeo.com is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with help.eyeo.com.  If not, see <http://www.gnu.org/licenses/>.
 */

const gulp = require("gulp");
const gutil = require("gulp-util");
const sourcemaps = require("gulp-sourcemaps");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const scss = require("postcss-scss");
const autoprefixer = require("autoprefixer");
const minify = require("gulp-minify");
const imagemin = require('gulp-imagemin');

/******************************************************************************
 * CSS
 ******************************************************************************/

gulp.task("css", function() {
  return gulp.src("./static/src/scss/main.scss")
    .pipe(sourcemaps.init())
    .pipe(postcss([autoprefixer()], {syntax: scss}).on("error", gutil.log))
    .pipe(sass({outputStyle: "compressed"}).on("error", gutil.log))
    .pipe(rename("main.min.css"))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./static/dist/css"));
});

/******************************************************************************
 * JS
 ******************************************************************************/

gulp.task("js", function() {
  return gulp.src(["./static/src/js/**/*.js"])
    .pipe(sourcemaps.init())
    .pipe(minify({
      noSource: true,
      ext: {min:".min.js"},
      preserveComments: "some"
    }))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./static/dist/js"))
});

/******************************************************************************
 * Images
 ******************************************************************************/

gulp.task("img", function() {
  return gulp.src(["./static/src/img/**"])
    .pipe(imagemin([
      imagemin.svgo({
        plugins: [
          {removeDimensions: false},
          {removeXMLNS: false},
          {cleanupIDs: true}
        ]
      })
    ]))
    .pipe(gulp.dest("./static/dist/img"));
});

/******************************************************************************
 * Watch
 ******************************************************************************/

gulp.task("watch", function() {
  gulp.watch("./static/src/scss/**/*.scss", ["css"]);
  gulp.watch("./static/src/js/**/*.js", ["js"]);
  gulp.watch("./static/src/img/**", ["img"]);
});

/******************************************************************************
 * Build
 ******************************************************************************/

gulp.task("build", ["css", "js", "img"]);

/******************************************************************************
 * Default
 ******************************************************************************/

gulp.task("default", ["build", "watch"]);
