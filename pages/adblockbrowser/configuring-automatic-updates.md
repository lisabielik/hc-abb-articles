title=Configuring automatic updates
description=Choose when to allow updates for Adblock Browser.
template=article
product_id=abb
category=Customization & Settings

<section class="platform-android" markdown="1">

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Tap **Configure automatic updates**.
5. Select one of the following:
  - **Non-metered Wi-Fi networks** (activated by default) - Updates the filter lists at any time, even when you're not connected to Wi-Fi.
  - **All Wi-Fi networks** - Updates the filter lists only when you are connected to Wi-Fi.
  - **Always** - Updates the filter lists at any time, using either Wi-Fi or mobile data.
</section>