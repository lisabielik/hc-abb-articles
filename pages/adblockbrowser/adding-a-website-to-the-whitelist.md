title=Adding a website to the whitelist
description=Add websites that you trust and want to support to your Adblock Browser whitelist. Ads will be shown on these websites.
template=article
product_id=abb
category=Customization & Settings

In general, a whitelist is a list of items that has been approved to receive special privileges. You can add website URLs to a whitelist in order to view ads on websites that you want to support.

<aside class="alert info" markdown="1">
**Important**: All ads, including those that do not adhere to the Acceptable Ads criteria, will be shown on whitelisted websites.
</aside>

<section class="platform-android" markdown="1">

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Tap **Whitelisted domains**.
5. Enter the website URL and tap the **+** icon.
<br>The website appears below *Whitelisted domains*.
</section>