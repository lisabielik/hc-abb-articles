title=Adjusting Adblock Browser notifications
description=Set your Adblock Browser for Android notification preferences.
template=article
product_id=abb
category=Customization & Settings

<section class="platform-android" markdown="1">

1. Open the Adblock Browser app.
2. Tap the **Android menu icon** and select **Settings**.
3. Tap **Notifications**.
<br>Adjust the notification settings to suit your preferences.
</section>