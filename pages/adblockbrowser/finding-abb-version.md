title=Checking your Adblock Browser for Android version
description=Add a preferred language to Adblock Browser for Android.
template=article
product_id=abb
category=Customization & Settings

<section class="platform-android" markdown="1">

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **About Adblock Browser**.
<br>The version number is displayed below *Application version*.
</section>