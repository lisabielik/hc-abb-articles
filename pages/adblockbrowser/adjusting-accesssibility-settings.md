title=Adjusting accessibility settings
description=Adjust Adblock Browser for Android accessibility settings, like text scaling.
template=article
product_id=abb
category=Customization & Settings

<section class="platform-android" markdown="1">

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Accessibility**.
4. Adjust the settings for one or more of the following:
  - **Text scaling** - Control text size by adjusting the slider to the left (smaller) or to the right (larger). When you double-tap text on a website, the text size should scale smaller or larger, depending on your slider preference.
  - **Force enable zoom** - Selected by default, this option prevents a website's request to automatically zoom in.
  - **Simplified view for webpages** - If selected, webpages will be presented in a "bare bones" format that focuses only on content.
</section>